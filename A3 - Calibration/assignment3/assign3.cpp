/****************** 
*  CSIS0317
* *****************
* Cory Norris
* cnorris@hku.hk
* 3035075858
*/





#include "assign3.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <cmath>
#include <fstream>
#include <iomanip>

using namespace std;
using namespace cv;

bool CalibrationHelper::LoadPoints(
	const char *filename, 
	Mat &points2D, 
	Mat &points3D
	) {
		ifstream in(filename);
		if (in.fail()) return false;

		points2D = Mat(0, 2, CV_32F);
		points3D = Mat(0, 3, CV_32F);
		float ix, iy, wx, wy, wz;

		while (in >> ix >> iy >> wx >> wy >> wz) {
			//Add points to 2D
			Mat pt2d(1, 2, CV_32F);			
			pt2d.at<float>(0, 0) = ix;
			pt2d.at<float>(0, 1) = iy;

			Mat pt3d(1, 3, CV_32F);
			pt3d.at<float>(0, 0) = wx;
			pt3d.at<float>(0, 1) = wy;
			pt3d.at<float>(0, 2) = wz;
			points2D.push_back(pt2d);
			points3D.push_back(pt3d);
		}

		in.close();
		return true;
}

//! add 2D/3D point pairs
bool CalibrationHelper::AddPoints(
	float ix,
	float iy,
	float wx,
	float wy,
	float wz,
	cv::Mat &points2D, 
	cv::Mat &points3D
	) {
		Mat pt2d(1, 2, CV_32F);
		Mat pt3d(1, 3, CV_32F);
		pt2d.at<float>(0, 0) = ix;
		pt2d.at<float>(0, 1) = iy;
		pt3d.at<float>(0, 0) = wx;
		pt3d.at<float>(0, 1) = wy;
		pt3d.at<float>(0, 2) = wz;
		points2D.push_back(pt2d);
		points3D.push_back(pt3d);
		return true;
}

std::vector<Point2D> CalibrationHelper::DetectCorners(
	const Mat &im
	) {
		std::vector<Point2D> result;
		Mat R, R_norm;
		//Step 1: obtaining R
		cornerHarris( im, R, 3, 3, 0.04, BORDER_DEFAULT);		
		normalize( R, R_norm, 0, 255, NORM_MINMAX, CV_32FC1, Mat());

		//Step 2: finding corners
		for (int xx = 1; xx < R.cols-1; ++xx) {
			for (int yy = 1; yy < R.rows-1; ++yy) {
				if (R.at<float>(yy,xx) > R.at<float>(yy-1,xx-1) &&
					R.at<float>(yy,xx) > R.at<float>(yy-1,xx) &&
					R.at<float>(yy,xx) > R.at<float>(yy-1,xx+1) &&
					R.at<float>(yy,xx) > R.at<float>(yy,xx-1) &&
					R.at<float>(yy,xx) > R.at<float>(yy,xx+1) &&
					R.at<float>(yy,xx) > R.at<float>(yy+1,xx-1) &&
					R.at<float>(yy,xx) > R.at<float>(yy+1,xx) &&
					R.at<float>(yy,xx) > R.at<float>(yy+1,xx+1)) {
						if (R_norm.at<float>(yy,xx) > 60) {
							Point2D tmp;
							float aa = R.at<float>(yy,xx) ;
							tmp.x = xx;
							tmp.y = yy;
							result.push_back(tmp);
						}
				}
			}
		}

		//Step 3: turning into sub-pixel precision with 1D approximation
		for (int ii = 0; ii < (int)result.size(); ++ii) {
			double f1, fn1, f0;
			int xx = result[ii].x, yy = result[ii].y;

			fn1 = R.at<float>(yy, xx-1);
			f1 = R.at<float>(yy, xx+1);
			f0 = R.at<float>(yy, xx);
			result[ii].x = - (f1 - fn1) / 2.0 / (f1 + fn1 - 2 * f0) + xx;
			fn1 = R.at<float>(yy-1, xx);
			f1 = R.at<float>(yy+1, xx);
			f0 = R.at<float>(yy, xx);
			result[ii].y = - (f1 - fn1) / 2.0 / (f1 + fn1 - 2 * f0) + yy;
		}

		return result;
}


/***
*  MY FUNCTIONS
*/
bool CalibrationHelper::ObtainProjectionMatrix(
	const Mat &points2D, 
	const Mat &points3D, 
	const std::vector<Point2D> &corners,
	Mat &projection
	) {		
		const int U = 0, V = 1;
		const int X = 0, Y = 1, Z = 2;

		/***********************************************************
		*				POINT-TO-PLANE PROJECTIVITY
		***********************************************************/
		//Since w = P^p * X^p
		// where
		// w =	[su		P =	[P00 P01 P02 P03	X = [X
		//		 sv			 P10 P11 P12 P13		 Y
		//		  s]		 P20 P21 P22 P23]		 Z
		//											 1]
		// but since, the Y coordinate in X is 0,
		// we can eliminate the second column in P.
		//
		// We can further set equations for su, sv and s
		// as follows
		//  su = P00X + P02Z + P03
		//  sv = P10X + P12Z + P13
		//  v  = P20X + P22Z + P23
		//
		// Subsequently dividing su by v, and sv by v
		// and subtracting setting the result to 0, we get:
		// P00X + P02Z + P03 - P20Xu - P22Zu - P23u = 0
		// P10X + P12Z + P13 - P20Xv - P22Zv - P23v = 0
		// However, we can ignore scaling factor, eg
		// P23 = 1 (eliminating the last value in our formula
		//
		// Finally, we can construct An*8, X and b
		// in order to solve for X



		//Solve for the FIRST plane, as described above
		//Populate A
		Mat matN = Mat(8, 8, CV_32F);
		for (int rowToFill = 0, rowToRead=0; rowToFill < 8; rowToFill+=2, rowToRead++) 
		{
			//first row
			int i=0;
			matN.at<float>(rowToFill, i++) = points3D.at<float>(rowToRead,X);
			matN.at<float>(rowToFill, i++) = points3D.at<float>(rowToRead,Z);
			matN.at<float>(rowToFill, i++) = 1;
			matN.at<float>(rowToFill, i++) = 0;
			matN.at<float>(rowToFill, i++) = 0;
			matN.at<float>(rowToFill, i++) = 0;
			matN.at<float>(rowToFill, i++) = -1 * points3D.at<float>(rowToRead,X) * 
				points2D.at<float>(rowToRead,U);
			matN.at<float>(rowToFill, i++) = -1 * points3D.at<float>(rowToRead,Z) * 
				points2D.at<float>(rowToRead,U);

			//second row
			i=0;
			matN.at<float>(rowToFill+1, i++) = 0;
			matN.at<float>(rowToFill+1, i++) = 0;
			matN.at<float>(rowToFill+1, i++) = 0;
			matN.at<float>(rowToFill+1, i++) = points3D.at<float>(rowToRead,X);
			matN.at<float>(rowToFill+1, i++) = points3D.at<float>(rowToRead,Z);
			matN.at<float>(rowToFill+1, i++) = 1;
			matN.at<float>(rowToFill+1, i++) = -1 * points3D.at<float>(rowToRead,X) * 
				points2D.at<float>(rowToRead,V);
			matN.at<float>(rowToFill+1, i++) = -1 * points3D.at<float>(rowToRead,Z) * 
				points2D.at<float>(rowToRead,V);


		}


		//Populate b
		Mat matB = Mat(8, 1, CV_32F);
		for (int rowToFill = 0, rowToRead=0; rowToFill < 8; rowToFill+=2, rowToRead++) 
		{
			matB.at<float>(rowToFill) = points2D.at<float>(rowToRead,U);
			matB.at<float>(rowToFill+1) = points2D.at<float>(rowToRead,V);
		}

		//Use solve to determine X1
		Mat matX1 = Mat();
		solve(matN,matB, matX1);


		//Solve for the SECOND plane, as described above
		//Populate A
		matN = Mat(8, 8, CV_32F);
		for (int rowToFill = 0, rowToRead=4; rowToFill < 8; rowToFill+=2, rowToRead++) 
		{
			//first row
			int i=0;
			matN.at<float>(rowToFill, i++) = points3D.at<float>(rowToRead,Y);
			matN.at<float>(rowToFill, i++) = points3D.at<float>(rowToRead,Z);
			matN.at<float>(rowToFill, i++) = 1;
			matN.at<float>(rowToFill, i++) = 0;
			matN.at<float>(rowToFill, i++) = 0;
			matN.at<float>(rowToFill, i++) = 0;
			matN.at<float>(rowToFill, i++) = -1 * points3D.at<float>(rowToRead,Y) * 
				points2D.at<float>(rowToRead,U);
			matN.at<float>(rowToFill, i++) = -1 * points3D.at<float>(rowToRead,Z) * 
				points2D.at<float>(rowToRead,U);

			//second row
			i=0;
			matN.at<float>(rowToFill+1, i++) = 0;
			matN.at<float>(rowToFill+1, i++) = 0;
			matN.at<float>(rowToFill+1, i++) = 0;
			matN.at<float>(rowToFill+1, i++) = points3D.at<float>(rowToRead,Y);
			matN.at<float>(rowToFill+1, i++) = points3D.at<float>(rowToRead,Z);
			matN.at<float>(rowToFill+1, i++) = 1;
			matN.at<float>(rowToFill+1, i++) = -1 * points3D.at<float>(rowToRead,Y) * 
				points2D.at<float>(rowToRead,V);
			matN.at<float>(rowToFill+1, i++) = -1 * points3D.at<float>(rowToRead,Z) * 
				points2D.at<float>(rowToRead,V);


		}


		//Populate b
		matB = Mat(8, 1, CV_32F);
		for (int rowToFill = 0, rowToRead=4; rowToFill < 8; rowToFill+=2, rowToRead++) 
		{
			matB.at<float>(rowToFill) = points2D.at<float>(rowToRead,U);
			matB.at<float>(rowToFill+1) = points2D.at<float>(rowToRead,V);
		}

		//Use solve to determine X2
		Mat matX2 = Mat();
		solve(matN, matB, matX2);



		//Reshape the matrixes
		//go from	[p01		to		[p00 p01 p02
		//			 p02				 p10 p11 p12
		//			 p10				 p20 p21 p22]
		//			 ...
		//			 p22]

		Mat projX = Mat(3,3, CV_32F);
		Mat projY = Mat(3,3, CV_32F);
		for (int row = 0; row < projX.rows; row++)
		{
			for (int col = 0; col < projX.cols; col++)
			{

				int n = col+row*projX.rows;
				if (n == 8) { //if it's the last element, set it to 1
					projX.at<float>(row,col) = 1;
					projY.at<float>(row,col) = 1;
				} else {
					projX.at<float>(row,col) = matX1.at<float>(n,0);
					projY.at<float>(row,col) = matX2.at<float>(n,0);
				}
			}
		}


		/***********************************************************
		*				GENERATING POINT PAIRS
		***********************************************************/
		// Use the projection matrix to translate real world  3D 
		// coordinates onto 2D points
		Mat A = Mat(0,12,CV_32F);

		//find the bottom left point in points3D
		for (float n = 0.5; n <= 9.5; n+=1.0)
		{
			for (float t = 0.5; t <= 7.5; t+=1.0)
			{
				//Translate each 3d point to 2d
				Mat pt3d = Mat(3,1,CV_32F);
				Mat pt2d = Mat(3,1,CV_32F);

				pt3d.at<float>(0,0) = n; //x (goes to 9.5)
				pt3d.at<float>(1,0) = t; //z (goes to 7.5)
				pt3d.at<float>(2,0) = 1; //1

				pt2d = projX * pt3d;
				pt2d.at<float>(0,0) = float(pt2d.at<float>(0,0)) / float(pt2d.at<float>(2,0));
				pt2d.at<float>(1,0) = float(pt2d.at<float>(1,0)) / float(pt2d.at<float>(2,0));

				//Snap the 2D points to a corner
				int iMinDist = 0;
				for(int i = 1; i < corners.size(); i++)
				{
					double newDist = 0, oldDist = 0;

					oldDist = sqrt(	pow((corners[iMinDist].x -pt2d.at<float>(0,0)),2)
						+ 
						pow((corners[iMinDist].y - pt2d.at<float>(1,0)),2));

					newDist = sqrt(	pow((corners[i].x - pt2d.at<float>(0,0)),2)
						+ 
						pow((corners[i].y - pt2d.at<float>(1,0)),2));

					if (newDist < oldDist)
						iMinDist = i;
				}
				pt2d.at<float>(0,0) = corners[iMinDist].x;
				pt2d.at<float>(1,0) = corners[iMinDist].y;

				// Put each row into a 1x12 matrix
				Mat row1xz = Mat(1, 12, CV_32F);
				Mat row2xz = Mat(1, 12, CV_32F);
				

				//ADD THE FIRST ROW	FOR X-Z	
				int i=0;
				row1xz.at<float>(0, i++) = pt3d.at<float>(0,0);
				row1xz.at<float>(0, i++) = 0;
				row1xz.at<float>(0, i++) = pt3d.at<float>(1,0);
				row1xz.at<float>(0, i++) = 1;

				row1xz.at<float>(0, i++) = 0;
				row1xz.at<float>(0, i++) = 0;
				row1xz.at<float>(0, i++) = 0;
				row1xz.at<float>(0, i++) = 0;

				row1xz.at<float>(0, i++) = -1 * pt2d.at<float>(U,0) * pt3d.at<float>(0,0);
				row1xz.at<float>(0, i++) = 0;
				row1xz.at<float>(0, i++) = -1 * pt2d.at<float>(U,0) * pt3d.at<float>(1,0);
				row1xz.at<float>(0, i++) = -1 * pt2d.at<float>(U,0);

				//ADD THE SECOND ROW FOR X-Z
				i=0;
				row2xz.at<float>(0, i++) = 0;
				row2xz.at<float>(0, i++) = 0;
				row2xz.at<float>(0, i++) = 0;
				row2xz.at<float>(0, i++) = 0;

				row2xz.at<float>(0, i++) = pt3d.at<float>(0,0);
				row2xz.at<float>(0, i++) = 0;
				row2xz.at<float>(0, i++) = pt3d.at<float>(1,0);
				row2xz.at<float>(0, i++) = 1;

				row2xz.at<float>(0, i++) = -1 * pt2d.at<float>(V,0) * pt3d.at<float>(0,0);
				row2xz.at<float>(0, i++) = 0;
				row2xz.at<float>(0, i++) = -1 * pt2d.at<float>(V,0) * pt3d.at<float>(1,0);
				row2xz.at<float>(0, i++) = -1 * pt2d.at<float>(V,0);

				//Fill the second row of the projectionMatrix
				pt2d =  projY * pt3d;
				pt2d.at<float>(0,0) = float(pt2d.at<float>(0,0)) / float(pt2d.at<float>(2,0));
				pt2d.at<float>(1,0) = float(pt2d.at<float>(1,0)) / float(pt2d.at<float>(2,0));

				//Snap the 2D points to a corner
				iMinDist = 0;
				for(int i = 1; i < corners.size(); i++)
				{
					double newDist = 0, oldDist = 0;

					oldDist = sqrt(	pow((corners[iMinDist].x -pt2d.at<float>(0,0)),2)
						+ 
						pow((corners[iMinDist].y - pt2d.at<float>(1,0)),2));

					newDist = sqrt(	pow((corners[i].x - pt2d.at<float>(0,0)),2)
						+ 
						pow((corners[i].y - pt2d.at<float>(1,0)),2));

					if (newDist < oldDist)
						iMinDist = i;
				}
				pt2d.at<float>(0,0) = corners[iMinDist].x;
				pt2d.at<float>(1,0) = corners[iMinDist].y;


				//ADD THE FIRST ROW FOR Y-Z
				i=0;
				Mat row1yz = Mat(1, 12, CV_32F);
				Mat row2yz = Mat(1, 12, CV_32F);


				row1yz.at<float>(0, i++) = 0;
				row1yz.at<float>(0, i++) = pt3d.at<float>(0,0);
				row1yz.at<float>(0, i++) = pt3d.at<float>(1,0);
				row1yz.at<float>(0, i++) = 1;

				row1yz.at<float>(0, i++) = 0;
				row1yz.at<float>(0, i++) = 0;
				row1yz.at<float>(0, i++) = 0;
				row1yz.at<float>(0, i++) = 0;

				row1yz.at<float>(0, i++) = 0;
				row1yz.at<float>(0, i++) = -1 * pt2d.at<float>(U,0) * pt3d.at<float>(0,0);
				row1yz.at<float>(0, i++) = -1 * pt2d.at<float>(U,0) * pt3d.at<float>(1,0);
				row1yz.at<float>(0, i++) = -1 * pt2d.at<float>(U,0);


				//ADD THE SECOND ROW FOR Y-Z
				i=0;
				row2yz.at<float>(0, i++) = 0;
				row2yz.at<float>(0, i++) = 0;
				row2yz.at<float>(0, i++) = 0;
				row2yz.at<float>(0, i++) = 0;

				row2yz.at<float>(0, i++) = 0;
				row2yz.at<float>(0, i++) = pt3d.at<float>(0,0);
				row2yz.at<float>(0, i++) = pt3d.at<float>(1,0);
				row2yz.at<float>(0, i++) = 1;

				row2yz.at<float>(0, i++) = 0;
				row2yz.at<float>(0, i++) = -1 * pt2d.at<float>(V,0) * pt3d.at<float>(0,0);
				row2yz.at<float>(0, i++) = -1 * pt2d.at<float>(V,0) * pt3d.at<float>(1,0);
				row2yz.at<float>(0, i++) = -1 * pt2d.at<float>(V,0);


				//add that matrix to A
				A.push_back(row1xz);
				A.push_back(row2xz);
				A.push_back(row1yz);
				A.push_back(row2yz);


			}
		}
		cout<<endl;

		//Matrixes to store interim values from decomposition		
		Mat w = Mat();
		Mat u= Mat();
		Mat vt = Mat();
		Mat v = Mat();

		//perform single value decomposition on project
		SVD::compute(A, w, u, vt);

		//transpose vt into v
		transpose(vt,v);
		Mat lastCol = v.col(v.cols-1);


		//reform & normalize the matrix so that the bottom right is 1	
		projection = Mat(3,4,CV_32F);
		
		for (int i = 0; i < projection.rows; i++)
		{
			for (int t = 0; t < projection.cols; t++)
			{
				int it = t+i*projection.cols;

				lastCol.at<float>(it,0) /=
					lastCol.at<float>(lastCol.rows-1,0);					;


				projection.at<float>(i,t) = lastCol.at<float>(it,0);
				//printf("%f, ",projection.at<float>(i,t));
			}
			cout<<endl;
		}
		return true;
}

bool CalibrationHelper::DecomposeProjectionMatrix(
	const Mat &projection,
	Mat &extrinsic,
	Mat &intrinsic
	) {
		//Decompose our matrix into rotation and translation
		Mat matRotation = Mat();
		
		//Get a 3x3 submatrix of projection and decompose it
		Mat matSubProj =  projection.colRange(0, projection.cols-1);
		RQDecomp3x3(matSubProj, intrinsic, matRotation);

		//Scale our intrinsic matrix
		float scalingFactor =  intrinsic.at<float>(intrinsic.rows -1,intrinsic.cols-1);
		intrinsic /= scalingFactor;		

		//Calculate the translation matrix using p3 and the inverse intrinsic matrix		
		Mat matTranslation = intrinsic.inv()* projection.col(projection.cols-1);
		matTranslation /= scalingFactor;
		
		for (int row = 0; row < matRotation.rows; row++)
		{
			extrinsic.at<float>(row,0) = matRotation.at<float>(row,0);
			extrinsic.at<float>(row,1) = matRotation.at<float>(row,1);
			extrinsic.at<float>(row,2) = matRotation.at<float>(row,2);
			extrinsic.at<float>(row,3) = matTranslation.at<float>(row,0);
		}

		return true;
}




/***
*  END OF MY FUNCTIONS
*/



Mat CalibrationHelper::DrawPoints(
	const Mat &scene, 
	const Mat &points2D
	) {
		Mat vis;
		scene.copyTo(vis);
		//draw a circle around corners
		for (int ii = 0; ii < (int)points2D.rows; ++ii) {
			float ix = points2D.at<float>(ii, 0),
				iy = points2D.at<float>(ii, 1);
			circle( vis, Point( ix, iy), 5,  Scalar(0), 2, 8, 0 );
		}
		return vis.clone();
		return scene.clone();
}

Mat CalibrationHelper::DrawPoints(
	const Mat &scene, 
	const std::vector<Point2D> &corners
	) {
		Mat vis;
		scene.copyTo(vis);
		//draw a circle around corners
		for (int ii = 0; ii < (int)corners.size(); ++ii) {
			Point2D tmp = corners[ii];
			circle( vis, Point( tmp.x, tmp.y), 5,  Scalar(0), 2, 8, 0 );
			//cout << tmp.x << "\t" << tmp.y << endl;
		}
		return vis.clone();
		return scene.clone();
}

bool CalibrationHelper::SaveResults(
	const Mat &projection,
	const Mat &extrinsic,
	const Mat &intrinsic,	
	const char* filename
	) {
		ofstream out(filename);
		if (out.fail()) return false;

		// output projection matrix
		out << "Projection Matrix" << endl;
		for (int ii = 0; ii < projection.rows; ++ii) {
			for (int jj = 0; jj < projection.cols; ++jj) {
				out << projection.at<float>(ii, jj) << " ";
			}
			out << endl;
		}

		// output extrinsic matrix
		out << "Extrinsic Matrix" << endl;
		for (int ii = 0; ii < extrinsic.rows; ++ii) {
			for (int jj = 0; jj < extrinsic.cols; ++jj) {
				out << extrinsic.at<float>(ii, jj) << " ";
			}
			out << endl;
		}

		// output intrinsic matrix
		out << "Intrinsic Matrix" << endl;
		for (int ii = 0; ii < intrinsic.rows; ++ii) {
			for (int jj = 0; jj < intrinsic.cols; ++jj) {
				out << intrinsic.at<float>(ii, jj) << " ";
			}
			out << endl;
		}

		return true;
}
