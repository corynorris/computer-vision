#ifndef ASSIGN3_H
#define ASSIGN3_H

#include <opencv\cv.h>
#include <vector>

//! struct for 2D points
struct Point2D {
	float x;
	float y;
};

//! Helper class for camera calibration
class CalibrationHelper {
public:
	//! load 2D/3D point pairs from file
	static bool LoadPoints(
		const char *filename, 
		cv::Mat &points2D, 
		cv::Mat &points3D
		);

	//! add 2D/3D point pairs
	static bool AddPoints(
		float ix,
		float iy,
		float wx,
		float wy,
		float wz,
		cv::Mat &points2D, 
		cv::Mat &points3D
		);

	//! provide corners in sub-pixel accuracy
	static std::vector<Point2D> DetectCorners(
		const cv::Mat &scene
		);

	//! You are required to finish this function
	//!		1. Because the markings of 2D points may not be accurate, you need to 
	//!		   align them to its nearest neighbour corner.
	//!		2. Find more corners and assign them 3D positions, using the knowledge 
	//!		   provided in our assignment sheet, based on plane-to-plane projectivties
    //!        to generate more 2D/3D point pairs.
	//!		3. Compute the least square solution for the projection matrix.
	static bool ObtainProjectionMatrix(
		const cv::Mat &points2D, 
		const cv::Mat &points3D, 
		const std::vector<Point2D> &corners,
		cv::Mat &projection
		);

	//! You are required to finish the function
	//!		Use the knowledge in lecture notes to finish this.
	static bool DecomposeProjectionMatrix(
		const cv::Mat &projection,
		cv::Mat &extrinsic,
		cv::Mat &intrinsic
		);	

	//! Draw manually marked points on the image
	static cv::Mat DrawPoints(
		const cv::Mat &scene, 
		const cv::Mat &points2D
		);

	//! Draw corner points on the image
	static cv::Mat DrawPoints(
		const cv::Mat &scene, 
		const std::vector<Point2D> &corners
		);

	//! Save projection matrix, extrinsic matrix and intrinsic matrix
	//! to the file.
	static bool SaveResults(
		const cv::Mat &projection,
		const cv::Mat &extrinsic,
		const cv::Mat &intrinsic,		
		const char* filename
		);
};


#endif