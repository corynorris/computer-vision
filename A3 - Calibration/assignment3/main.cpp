#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "assign3.h"

using namespace cv;
using namespace std;

//! mouse event callback function
static void onMouse(int event, int x, int y, int, void*);

static float g_ix;
static float g_iy;
static float g_wx;
static float g_wy;
static float g_wz;
static Mat g_pt2d;
static Mat g_pt3d;
static int g_counter;


int main()
{
	Mat tmp, proj(3, 4, CV_32F), intr(3, 3, CV_32F), extr(3, 4, CV_32F), imt;
	vector<Point2D> corners;
	
	g_pt2d.create(0, 2, CV_32F);
	g_pt3d.create(0, 3, CV_32F);
	g_counter = 0;
	/*
	//perform calibration for scene 1
	cout << "Please select the 4 corners of the first calibration grid: " << endl;
	Mat im1 = imread("scene1.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	Mat imc1 = imread("scene1.jpg", CV_LOAD_IMAGE_COLOR);
	namedWindow("Image1");
	imshow("Image1", imc1);
	setMouseCallback( "Image1", onMouse, 0);
	waitKey(0);

	cout << "Showing the corners of the calibration grid." << endl;
	tmp = CalibrationHelper::DrawPoints(imc1, g_pt2d);
	imshow("Image1", tmp);
	waitKey(0);

	cout << "Showing the detected corners." << endl;
	corners = CalibrationHelper::DetectCorners(im1);
	imt = CalibrationHelper::DrawPoints(imc1, corners);
	imshow("Image1", imt);
	waitKey(0);

	cout << "Calibrating the camera." << endl;
	CalibrationHelper::ObtainProjectionMatrix(g_pt2d, g_pt3d, corners, proj);
	CalibrationHelper::DecomposeProjectionMatrix(proj, extr, intr);
	CalibrationHelper::SaveResults(proj, extr, intr, "calibration1.txt");
	cout << "Matrices are saved and calibration is finished." << endl;
	*/
	cout << "Processing the second case." << endl;
	//perform calibration for scene 2
	Mat im2 = imread("scene2.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	Mat imc2 = imread("scene2.jpg", CV_LOAD_IMAGE_COLOR);
	namedWindow("Image2");
	
	if (!CalibrationHelper::LoadPoints("markings2.txt", g_pt2d, g_pt3d)) {
		return -1;
	}
	corners = CalibrationHelper::DetectCorners(im2);
	imt = CalibrationHelper::DrawPoints(imc2, corners);
	CalibrationHelper::ObtainProjectionMatrix(g_pt2d, g_pt3d, corners, proj);
	CalibrationHelper::DecomposeProjectionMatrix(proj, extr, intr);
	CalibrationHelper::SaveResults(proj, extr, intr, "calibration2.txt");

	return 0;
}


void onMouse(int event, int x, int y, int, void*) {
	if (event == CV_EVENT_LBUTTONDOWN) {
		g_ix = x;
		g_iy = y;
		if (g_counter < 4) {
			cout << "Input 3D coordinates of this point for the 1st plane: ";
		} else if (g_counter < 8) {
			cout << "Input 3D coordinates of this point for the 2nd plane: ";
		} else {
			cout << "You have already marked 8 points for 2 planes" << endl;
			return;
		}
		cin >> g_wx >> g_wy >> g_wz;
		CalibrationHelper::AddPoints(g_ix, g_iy, g_wx, g_wy, g_wz, g_pt2d, g_pt3d);
		g_counter++;
		if (g_counter == 4) {
			cout << "Please select the 4 corners of the second calibration grid: " << endl;
		}
	} else {
		return;
	}
}