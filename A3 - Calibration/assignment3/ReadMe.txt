My code modifies the two functions to find projection matrix using every possible corner in the given image, both on the X-Z and Y-Z plane.
Then it decomposes the matrix into it's internal (camera) component and its external (rigid body motion) component.
The code is heavliy documented, including the math and theory behind the math.