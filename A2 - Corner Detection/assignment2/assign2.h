#ifndef ASSIGN2_H
#define ASSIGN2_H

#include <opencv\cv.h>
#include <vector>

struct Point2D {
	double x;
	double y;
};

class CornerDetector {
public:
	//! you are required to complete this function
	//!	
	//! parameters:
	//!		im - input gray image
	//!		sigma - parameters for 1D Gaussian, see our lecture notes
	//!		threshold - threshold for R value
	static std::vector<Point2D> DetectCorners(const cv::Mat &im, double sigma, double threshold);

	//! this function helps you understand the input and output parameters
	static std::vector<Point2D> HarrisDemo(const cv::Mat &im);

	//! draw corners on the reference image
	static cv::Mat DrawCorners(const cv::Mat &im, const std::vector<Point2D> &corners);
};

#endif