#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "assign2.h"

using namespace cv;

int main()
{
	Mat im = imread("sample.jpg", CV_LOAD_IMAGE_GRAYSCALE);
	Mat imc = imread("sample.jpg", CV_LOAD_IMAGE_COLOR);
	namedWindow("Image");
	imshow("Image", im);
	waitKey(0);

	std::vector<Point2D> demo_corners = CornerDetector::HarrisDemo(im);
	Mat demo_result = CornerDetector::DrawCorners(imc, demo_corners);
	namedWindow("Demo");
	imshow("Demo", demo_result);
	waitKey(0);


	std::vector<Point2D> corners = CornerDetector::DetectCorners(im, 1, 200);
	Mat result = CornerDetector::DrawCorners(imc, corners);
	namedWindow("Your Result");
	imshow("Your Result", result);
	waitKey(0);

	return 0;
}
