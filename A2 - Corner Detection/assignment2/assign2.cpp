/****************** 
*  CSIS0317
* *****************
* Cory Norris
* cnorris@hku.hk
* 3035075858
*/


#include "assign2.h"
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

using namespace std;

//! You are required to implement this function in Assignment 2
vector<Point2D> CornerDetector::DetectCorners(const cv::Mat &im, double sigma, double threshold) 
{
	int cols = im.cols;
	int rows = im.rows;
	int smoothSize = 3;
	std::vector<Point2D> result;
	
	//image as a float
	cv::Mat imr = cv::Mat(rows, cols, CV_32F);
	
	//interim matrixes
	cv::Mat iX = cv::Mat(rows, cols, CV_32F);
	cv::Mat iY = cv::Mat(rows, cols, CV_32F);
	cv::Mat iX2 = cv::Mat(rows, cols, CV_32F);
	cv::Mat iY2 = cv::Mat(rows, cols, CV_32F);
	cv::Mat iXY = cv::Mat(rows, cols, CV_32F);	

	//Corenerless
	cv::Mat R = cv::Mat(rows, cols, CV_32F);



	cv::GaussianBlur(im,im,cv::Size(smoothSize,smoothSize),sigma,sigma, cv::BORDER_REPLICATE);


	im.convertTo(imr, CV_32F);

	for (int curCol = 0; curCol < cols; ++curCol) {
		for (int curRow = 0; curRow < rows; ++curRow) {

			float col = 0.0f;
			float row = 0.0f;
			float sumCol = 0.0f;
			float sumRow = 0.0f;

			//imr.at<float>(curRow, curCol) = imr.at<float>(curRow, curCol)/255;
			//cout<<imr.at<float>(curRow, curCol)<<endl; 

			//consider the edge problem
			//use padding to deal with it

			for (int m = -1; m <= 1; ++m) {
				
				//make sure the X value is in bounds
				if ((curCol+m < 0) || ((curCol+m) >= cols))		
					col = imr.at<float>(curRow, curCol); 
				else
					col = imr.at<float>(curRow, curCol+m);

				//make sure the Y value is in bounds
				if ((curRow+m < 0) || ((curRow+m) >= rows)) 				
					row = imr.at<float>(curRow, curCol); 
				else
					row = imr.at<float>(curRow+m, curCol);

				//Apply the filters [1 0 -1]	
				int neg_m = m*-1;
				col = col *neg_m;
				row = row *neg_m;
				
				//sum the x & y
				sumCol += col;
				sumRow += row;

			}
		
			iX.at<float>(curRow,curCol) = (sumCol/2);
			iY.at<float>(curRow,curCol) = (sumRow/2);	
			
			

			
		}
	}


	//Form images Ix^2, Iy^2 and IxIy
	cv::multiply(iX,iX,iX2);
	cv::multiply(iY,iY,iY2);
	cv::multiply(iX,iY,iXY);
	

	
	//Guassian Blur
	cv::GaussianBlur(iXY,iXY,cv::Size(smoothSize,smoothSize),sigma,sigma, cv::BORDER_REPLICATE);
	cv::GaussianBlur(iX2,iX2,cv::Size(smoothSize,smoothSize),sigma,sigma, cv::BORDER_REPLICATE);
	cv::GaussianBlur(iY2,iY2,cv::Size(smoothSize,smoothSize),sigma,sigma, cv::BORDER_REPLICATE);



	//matrix determinant
	//[a b]  -> [iX2  iXY]
	//[c d]  -> [iXY  iY2]
	//ad-bc	 -> [iX2*iY2 - iXY^2]\

	float max = 0;
	float min = 0;

	for (int curCol = 0; curCol < cols; ++curCol) {
		for (int curRow = 0; curRow < rows; ++curRow) {

			float det = (iX2.at<float>(curRow, curCol) *
						iY2.at<float>(curRow, curCol)) -
						(iXY.at<float>(curRow, curCol) *
						iXY.at<float>(curRow, curCol));

			float trace = (iX2.at<float>(curRow, curCol) +iY2.at<float>(curRow, curCol));

			R.at<float>(curRow,curCol) = det - 0.04*(trace*trace);
			if (R.at<float>(curRow,curCol) > max) 
				max = R.at<float>(curRow,curCol);
			if (R.at<float>(curRow,curCol) < min)
				min = R.at<float>(curRow,curCol);
		}
	}

	

	//Detect Local Maxima
	cv::Mat normR = cv::Mat(rows, cols, CV_8UC1);
	
	/*R.convertTo(normR,CV_8UC1);
	cv::imshow("R", R);
	cv::waitKey(0);
	cv::imshow("normR", normR);
	cv::waitKey(0);*/


	for (int curCol = 1; curCol < cols-1; ++curCol) {
		for (int curRow = 1; curRow < rows-1; ++curRow) {
			//cout<<R.at<float>(curRow, curCol)<<endl; 
			
			
			if ((R.at<float>(curRow,curCol) > R.at<float>(curRow-1,curCol-1)) &&
				(R.at<float>(curRow,curCol) > R.at<float>(curRow-1,curCol)) &&
				(R.at<float>(curRow,curCol) > R.at<float>(curRow-1,curCol+1)) &&
				(R.at<float>(curRow,curCol) > R.at<float>(curRow,curCol-1)) &&
				
				(R.at<float>(curRow,curCol) > R.at<float>(curRow,curCol+1)) &&
				(R.at<float>(curRow,curCol) > R.at<float>(curRow+1,curCol-1)) &&
				(R.at<float>(curRow,curCol) > R.at<float>(curRow+1,curCol)) &&
				(R.at<float>(curRow,curCol) > R.at<float>(curRow+1,curCol+1)) )
			{

				float norm = (R.at<float>(curRow,curCol) - min) / (max-min);
				norm *= 255;
				if (norm >threshold) {
					Point2D tmp;
					tmp.x = curCol;
					tmp.y = curRow;
					result.push_back(tmp);
				}
			}

		}
	}


	/*
	cv::Mat disp = cv::Mat(rows, cols, CV_8UC1);
	
	cv::convertScaleAbs(iX,disp);
	cv::imshow("iX", disp);
	cv::waitKey(0);

	cv::convertScaleAbs(iY,disp);
	cv::imshow("iY", disp);
	cv::waitKey(0);

	cv::convertScaleAbs(iX2,disp);
	cv::imshow("iX2", disp);
	cv::waitKey(0);

	cv::convertScaleAbs(iY2,disp);
	cv::imshow("iY2", disp);
	cv::waitKey(0);

	cv::convertScaleAbs(iXY,disp);
	cv::imshow("iXY", disp);
	cv::waitKey(0);

	cv::convertScaleAbs(R,disp);
	cv::imshow("iXY", disp);
	cv::waitKey(0);*/

	return result;
}

//! This function detects the corners according to corner responce.
//! It is provided for you to better understand input and output format.
//! You are required to implement the DetectCorners(const cv::Mat &) by 
//! yourself step by step.
vector<Point2D> CornerDetector::HarrisDemo(const cv::Mat &im)
{
	cv::Mat dst, dst_norm, dst_norm_scaled;
	std::vector<Point2D> result;
	int thresh = 80;

	// Detector parameters
	// These are parameters for OpenCV built-in corner detector
	// You can omit them.
	int blockSize = 3;
	int apertureSize = 3;
	double k = 0.04;

	// Detect corners
	cv::cornerHarris( im, dst, blockSize, apertureSize, k, cv::BORDER_DEFAULT);

	// Normalize the responce
	cv::normalize( dst, dst_norm, 0, 255, cv::NORM_MINMAX, CV_32FC1, cv::Mat());
	cv::convertScaleAbs( dst_norm, dst_norm_scaled );

	//cv::imshow("THERES", im);
	//cv::waitKey(0);

	// Save the points without considering non-maximum supression 
	// and sub-pixel accuracy.
	for( int j = 0; j < dst_norm.rows ; j++ ) {
		for( int i = 0; i < dst_norm.cols; i++ ) {
			if( (int) dst_norm.at<float>(j,i) > thresh ) {
				Point2D tmp;
				tmp.x = i;
				tmp.y = j;
				result.push_back(tmp);
			}
		}
	}
	return result;
}

//! draw corners on the reference image
cv::Mat CornerDetector::DrawCorners(const cv::Mat &im, const vector<Point2D> &corners)
{
	cv::Mat vis;
	im.copyTo(vis);
	//draw a circle around corners
	for (int ii = 0; ii < (int)corners.size(); ++ii) {
		Point2D tmp = corners[ii];
		cv::circle( vis, cv::Point( tmp.x, tmp.y), 5,  cv::Scalar(0), 2, 8, 0 );
	}
	return vis.clone();
}


