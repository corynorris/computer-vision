#include <opencv/cv.h>
#include <opencv/highgui.h>
#include "assign1.h"

int main()
{
	cv::Mat im = cv::imread("lenna.png");
	cv::namedWindow("Lenna");
	cv::imshow("Lenna", im);
	cv::waitKey(0);

	cv::Mat im_gray = ImageProcessor::ConvertToMonochrome(im);
	cv::namedWindow("Gray");
	cv::imshow("Gray", im_gray);
	cv::waitKey(0);
	cv::imwrite("lenna_gray.png", im_gray);

	cv::Mat im_negative = ImageProcessor::NegativeTransformation(im_gray);
	cv::namedWindow("NegativeTransformation");
	cv::imshow("NegativeTransformation", im_negative);
	cv::waitKey(0);
	cv::imwrite("lenna_negative.png", im_negative);

	cv::Mat im_laplacian = ImageProcessor::LaplacianFilter(im_gray);
	cv::namedWindow("Laplacian");
	cv::imshow("Laplacian", im_laplacian);
	cv::waitKey(0);
	cv::imwrite("lenna_laplacian.png", im_laplacian);

	//! this is the demo for box smoothing
	cv::Mat im_gray_demo = cv::imread( "lenna.png", CV_LOAD_IMAGE_GRAYSCALE);
	cv::Mat im_box = ImageProcessor::BoxSmoothFilter(im_gray_demo);
	cv::namedWindow("Box-smoothing");
	cv::imshow("Box-smoothing", im_box);
	cv::waitKey(0);
	cv::imwrite("lenna_smooth.png", im_box);
	return 0;
}