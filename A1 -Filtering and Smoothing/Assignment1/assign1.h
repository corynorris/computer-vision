#ifndef ASSIGN1_H
#define ASSIGN1_H

#include<opencv\cv.h>

class ImageProcessor {
public:
	//! Convert rgb image to gray image using YIQ model
	static cv::Mat ConvertToMonochrome(const cv::Mat &im);
	//! Negative transformation on a gray image
	static cv::Mat NegativeTransformation(const cv::Mat &im);
	//! 3x3 Laplacian filter on a gray image
	static cv::Mat LaplacianFilter(const cv::Mat &im);

	//! 3x3 Box-smoothing filter on a gray image
	//! This has already been implemented for reference.
	static cv::Mat BoxSmoothFilter(const cv::Mat &im);

private:
	//! get the pixel value considering edge problem for box-smoothing filter.
	static int At(const cv::Mat &im, int yy, int xx);
};

#endif