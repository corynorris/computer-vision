/**
* CSIS0317
* Cory Norris
* cnorris@hku.hk
* 3035075858
*/


#include "assign1.h"

//! Convert rgb image to gray image
cv::Mat ImageProcessor::ConvertToMonochrome(const cv::Mat &im) {
	//accessing pixels
	int width = im.cols;
	int height = im.rows;
	cv::Mat imr = cv::Mat(height, width, CV_8UC1);
	for (int yy = 0; yy < im.rows; ++yy) {
		for (int xx = 0; xx < im.cols; ++xx) {
			int b = im.at<cv::Vec3b>(yy,xx)[0];	//b
			int g = im.at<cv::Vec3b>(yy,xx)[1];	//g
			int r = im.at<cv::Vec3b>(yy,xx)[2];	//r
			
			double Y = 0.299*r + 0.587*g + 0.114*b;

			imr.at<uchar>(yy, xx) = int(Y);	//r
	
		}
	}
	return imr;
}

//! Negative transformation on a gray image
cv::Mat ImageProcessor::NegativeTransformation(const cv::Mat &im) {
	int width = im.cols;
	int height = im.rows;
	cv::Mat imr = cv::Mat(height, width, CV_8UC1);

	int sum;
	//begin accessing all pixels
	for (int xx = 0; xx < width; ++xx) {
		for (int yy = 0; yy < height; ++yy) {
			//the average as the filtered result.
			imr.at<uchar>(yy, xx) = 255 - im.at<uchar>(yy, xx);
		}
	}
	return imr;
	
}

//! 3x3 Laplacian filter on a gray image
cv::Mat ImageProcessor::LaplacianFilter(const cv::Mat &im) {
	int width = im.cols;
	int height = im.rows;
	cv::Mat imr = cv::Mat(height, width, CV_8UC1);

	int sum;
	//begin accessing all pixels (ignoring the edge pixels as instructed)
	for (int xx = 1; xx < width-1; ++xx) {
		for (int yy = 1; yy < height-1; ++yy) {
			//the average as the filtered result.
			imr.at<uchar>(yy, xx) = 255 - im.at<uchar>(yy, xx);
			int laplacenized = (im.at<uchar>(yy-1, xx-1) *-1) + (im.at<uchar>(yy-1, xx) *-1) + (im.at<uchar>(yy-1, xx+1) *-1)
							+  (im.at<uchar>(yy, xx-1) *-1)	  + (im.at<uchar>(yy, xx) * 8)   + (im.at<uchar>(yy, xx+1) *-1)
							+  (im.at<uchar>(yy+1, xx-1) *-1) + (im.at<uchar>(yy+1, xx) *-1) + (im.at<uchar>(yy+1, xx+1) *-1);
			if (laplacenized > 255) { laplacenized = 255; } else if (laplacenized < 0) { laplacenized = 0; }
			imr.at<uchar>(yy, xx) = laplacenized;
			}
	}
	return imr;
}

//! 3x3 Box-smoothing filter on a gray image
cv::Mat ImageProcessor::BoxSmoothFilter(const cv::Mat &im) {
	int width = im.cols;
	int height = im.rows;
	cv::Mat imr = cv::Mat(height, width, CV_8UC1);
	int sum;
	//begin accessing all pixels
	for (int xx = 0; xx < width; ++xx) {
		for (int yy = 0; yy < height; ++yy) {
			sum = 0;
			//consider the edge problem
			//use padding to deal with it
			for (int xm = 0; xm < 3; ++xm) {
				for (int ym = 0; ym < 3; ++ym) { 
					sum += At(im, yy+ym-1, xx+xm-1);
				}
			}
			//the average as the filtered result.
			imr.at<uchar>(yy, xx) = int(sum / 9.0);
		}
	}
	return imr;
}

int ImageProcessor::At(const cv::Mat &im, int yy, int xx) {
	int x, y;
	x = (xx < 0? -xx : xx);
	x = (xx >= im.cols? 2*im.cols-x-1 : x);
	y = (yy < 0? -yy : yy);
	y = (yy >= im.rows? 2*im.rows-y-1 : y);
	return im.at<uchar>(y, x);
}